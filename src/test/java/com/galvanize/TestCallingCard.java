package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCallingCard {

    @Test
    public void testToInstantiateCallingCardWithCentsPerMinute() {
        // Setup
        CallingCard card1 = new CallingCard(10);

        // Enact
        int centsPerMinuteResult = card1.centsPerMinute;

        // Assert
        assertEquals(10, centsPerMinuteResult);
    }

    @Test
    public void addDollarsAndGetDollarBalanceAndConvertDollarsBalanceToCents()
    {
        //Setup
        CallingCard card2 = new CallingCard(20);
        //Enact
        card2.addDollars(1);
        card2.getDollarsBalance();
        assertEquals(1, card2.getDollarsBalance());
        card2.addDollars(5);
        assertEquals(6, card2.getDollarsBalance());
        //Enact dollars to cents
        card2.getCentsBalance();
        assertEquals(600, card2.getCentsBalance());
        card2.addDollars(4);
        assertEquals(1000, card2.getCentsBalance());
    }
    @Test
    public void foo()
    {
        //Test to get remaining minutes
        //Setup

    }


}