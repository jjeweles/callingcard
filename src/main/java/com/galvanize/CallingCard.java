package com.galvanize;

public class CallingCard
{
    public int centsBalance;
    public int dollarsBalance;
    public int centsPerMinute;
    public CallingCard(int centsPerMinute)
    {
        this.centsPerMinute = centsPerMinute;
    }
    public void addDollars(int dollars)
    {
        this.dollarsBalance += dollars;
    }
    public int getDollarsBalance()
    {
        return dollarsBalance;
    }
    public int getCentsBalance()
    {
        return dollarsBalance *100;
    }
}
